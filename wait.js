/**
 * A function that blocks javascript execution.
 * This only exists for the purposes of simulation and would
 * never ever be needed in any real code.
 * @param {number} ms how long we should wait
 */
const wait = ms => {
  let start = new Date().getTime();
  let end = start;
  while (end < start + ms) {
    end = new Date().getTime();
  }
};

module.exports = wait