const main = async () => {
  /**
   * An operation that returns a very complex
   * number which is the solution for life, the universe,
   * and everything.
   *
   * @param { Function } done a function that should be called when the operation finishes
   * @returns { void } returns nothing, but calls the function when done
   **/
  const expensiveOperation = done => {
    console.log("computing answer...");
    const answer = 42;
    setTimeout(() => {
      done(answer);
    }, 1000);
  };

  console.log("--- 1-  start");

  expensiveOperation(answer => {
    console.log("--- 2 - got the answer:", answer);
  });

  console.log("--- 3 - end");
};

main();
