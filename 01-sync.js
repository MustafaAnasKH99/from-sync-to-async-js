const main = async () => {
  /**
   * An operation that returns a very complex
   * number which is the solution for life, the universe,
   * and everything.
   *
   * @returns { number } the answer to life, the universe, and everything
   **/
  const expensiveOperation = () => {
    console.log("computing answer...");
    const answer = 42;
    require("./wait")(1000);
    return answer;
  };

  console.log("--- 1 - start");

  const answer = expensiveOperation();
  console.log("--- 2 - got the answer:", answer);

  console.log("--- 3 - end");
};

main();
