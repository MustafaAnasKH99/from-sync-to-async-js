const main = async () => {
  /**
   * An operation that returns a very complex
   * number which is the solution for life, the universe,
   * and everything.
   *
   * @returns { Promise<number> } A promise containing the answer
   **/
  const expensiveOperation = () => {
    return new Promise((resolve, reject) => {
      console.log("computing answer...");
      const answer = 42;
      setTimeout(() => {
        resolve(answer);
      }, 1000);
    });
  };

  console.log("--- 1 - start");

  expensiveOperation().then(answer => {
    console.log("--- 2 - got the answer:", answer);
  });

  console.log("--- 3 - end");
};

main();
