# From Sync to Async

## How to

run each file one by one with `node` to see how it behaves

Disregard `wait.js` which is there only for simulation purposes

## Explanations

#### `01-sync.js`

This is regular synchronous code. If you run it, you'll see:

```sh
> node 01-sync.js
--- 1 - start
computing answer...
--- 2 - got the answer: 42
--- 3 - end
```

`computing answer...` will need 1 second before resuming.

Of course, if this was an operation you're running yourself on your own laptop, that's fine. But if this was a server, it means the next client will wait 1 second before getting an answer, while themselves will block the next user for 1 second. Three simultaneous clients will make the next client wait 3 seconds, and so on. What happens when you have a hundred clients? What happens if the operation needs 5 seconds? This is not an acceptable solution.

The answer to that is `asynchronous` code. Code that does not block the main execution.

Let's observe a few ways of doing that.

#### `02-callbacks.js`

```sh
> node 02-callbacks.js
--- 1 - start
computing answer...
--- 3 - end
--- 2 - got the answer: 42
```

As you notice, the `end` comes before the `answer`. The answer still needed a second, but the javascript execution was freed immediatly.

This is **non blocking** code.

If you observe the [code](./02-callbacks.js), you will see the following:

```js
  const expensiveOperation = done => {
    ... // do something expensive
      done(answer);
    ...
  };
...
  expensiveOperation( answer => {
    console.log("--- 2 - got the answer:", answer);
  });
```

`expensiveOperation` runs, and **when it is finished**, it calls the provided `done` function.

This is the `done` function that `expensiveOperation` runs:

```js
answer => {
  console.log("--- 2 - got the answer:", answer);
};
```

#### `03-promises.js`

Callbacks work very well, but they are:

1. A bit hard to read
2. A bit hard to remember to use
3. Can lead to "callback hell":

```js
const compute_a_result = (x,done) => {
    try{
        do_first_operation(x, (x2) => {
            do_second_operation(x2, (x3) => {
                do_third_operation(x3, (result) => {
                    done(result);  // 42
                });
            });
        });
    }catch(err){
        done(err)
    }
}

compute_a_result((result)=>{
    if(result instanceOf Error){
        console.error(result)
    }else{
        console.log(result)
    }
})
```

To solve some of those problems, Javascript has adopted _Promises_.

The same example above, if it was using promises, would read as:

```js
const compute_a_result = (x) => {
    do_first_operation(x)
    .then(do_second_operation)
    .then(do_third_operation)
}

compute_a_result()
    .then(result =>{
        console.log(result)
    })
    .catch( error => {
        console.error(error)
    })
})
```

A number of popular tools and libraries implement promises by default, such as `fetch`.

Promises are good and great, but they can still be confusing to read. Enters `async`.

#### `04-async.js`

`Async` is **syntaxic sugar**. That means it is not a new construct, but just *another way* of writing the same thing.

async functions **are** functions that return promises. You just write them without the `then` syntax. `async` gives the illusion of writing synchronous code, without blocking execution.

The example above, written with async syntax, would read as:

```js
const compute_a_result = async (x) => {
    const x2 = await do_first_operation(x)
    const x3 = await do_second_operation(x2)
    const x4 = await do_third_operation(x3)
    return x4
}

try{
    const result = await compute_a_result()
    console.log(result)
}catch(error){
    console.error(error)
}
```